//Global
var usersDB = [
{
  "id": 0,
  "name": "Alicia Rubio",
  "location": "Toronto",
  "photo" : "../www/img/0.jpg",
  "age" : 22,
  "description" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque faucibus magna id lacus tincidunt finibus."
},
{
  "id": 1,
  "name": "Aada Pulkkinen",
  "location": "Toronto",
  "photo" : "../www/img/1.jpg",
  "age" : 20,
  "description" : "Fusce nec ligula posuere, congue augue ut, egestas diam. Curabitur maximus mi et est egestas, fringilla pellentesque felis consectetur."
},
{
  "id": 2,
  "name": "Valerie Ross",
  "location": "Toronto",
  "photo" : "../www/img/2.jpg",
  "age" : 31,
  "description" : "Proin at felis eu urna maximus mollis. Curabitur et est ac tellus fringilla vestibulum."
},
{
  "id": 3,
  "name": "Leona Robinson",
  "location": "Toronto",
  "photo" : "../www/img/3.jpg",
  "age" : 25,
  "description" : "Aenean cursus mauris at libero rutrum, non iaculis nulla sagittis. Maecenas malesuada dui euismod leo scelerisque sagittis."
},
{
  "id": 4,
  "name": "Astrid Larsen",
  "location": "Toronto",
  "photo" : "../www/img/4.jpg",
  "age" : 21,
  "description" : "Integer in tortor quis justo auctor auctor vel vitae erat. In in enim porttitor, vehicula dui a, tristique nibh."
},
{
  "id": 5,
  "name": "Deborah Craig",
  "location": "Toronto",
  "photo" : "../www/img/5.jpg",
  "age" : 28,
  "description" : "Etiam viverra arcu non convallis interdum. Ut facilisis eros et feugiat accumsan."
},
{
  "id": 6,
  "name": "Aubrey Sirko",
  "location": "Toronto",
  "photo" : "../www/img/6.jpg",
  "age" : 33,
  "description" : "Donec ac nulla nec metus tristique porttitor eget id nisl. Etiam venenatis dolor ut augue feugiat, ornare blandit felis placerat."
},
{
  "id": 7,
  "name": "Lotta Wirtanen",
  "location": "Toronto",
  "photo" : "../www/img/7.jpg",
  "age" : 28,
  "description" : "Pellentesque tincidunt neque varius diam molestie, vulputate iaculis augue vehicula. Duis ut ipsum eget mi viverra dapibus id placerat lectus."
},
{
  "id": 8,
  "name": "Rosario Suarez",
  "location": "Toronto",
  "photo" : "../www/img/8.jpg",
  "age" : 23,
  "description" : "Morbi lobortis dui ut tellus rutrum, vitae luctus risus sodales. Morbi congue lectus quis neque ullamcorper cursus."
},
{
  "id": 9,
  "name": "Minea Arola",
  "location": "Toronto",
  "photo" : "../www/img/9.jpg",
  "age" : 37,
  "description" : "Donec ornare leo eu ligula varius faucibus. Vestibulum malesuada lacus quis ullamcorper posuere."
},
{
  "id": 10,
  "name": "Ruthild Rudolph",
  "location": "Toronto",
  "photo" : "../www/img/10.jpg",
  "age" : 26,
  "description" : "Duis sed felis eu magna blandit rutrum. Sed efficitur eros vel nunc elementum pharetra."
},
{
  "id": 11,
  "name": "Priscilla Price",
  "location": "Toronto",
  "photo" : "../www/img/11.jpg",
  "age" : 21,
  "description" : "Phasellus eu diam eleifend, suscipit lorem ut, tempor orci. Pellentesque vitae est in est accumsan ultricies."
},
{
  "id": 12,
  "name": "Rosária Vieira",
  "location": "montreal",
  "photo" : "../www/img/12.jpg",
  "age" : 28,
  "description" : "Nullam quis eros interdum, tincidunt est vitae, elementum arcu. Sed ac magna pellentesque, commodo felis vitae, finibus odio."
}
];
var userLocation = localStorage.getItem('userLocation'); // get this
var userLoogedId = localStorage.getItem('userID'); //get this
console.log("userLocation: "+ userLocation);
console.log("userLoogedId: "+ userLoogedId);
var usersDisliked = [];
var usersNearby = [];
var posCurrentUserProfile = null;
var isMobile = false;
//var db = null;

//like or dislike
document.getElementById("like").addEventListener("click", likeUser);
document.getElementById("dislike").addEventListener("click", dislikeUser)

// add event listeners
document.addEventListener("deviceReady", connectToDatabase);

var db = null;
db = window.openDatabase("tynder", "1.0", "Tynder App", 2 * 1024 * 1024);

function connectToDatabase() {
  console.log("device is ready - connecting to database");
  // 2. open the database. The code is depends on your platform!
  if (window.cordova.platformId === 'browser') {
    console.log("browser detected...");
    // For browsers, use this syntax:
      //(nameOfDb, version number, description, db size)
    // By default, set version to 1.0, and size to 2MB
  // db = window.openDatabase("Tinder", "1.0", "Tinder App",2 * 1024 * 1024);
  isMobile = false;
  }
  else {
    console.log("mobile device detected!");
    var databaseDetails = {"name":"Tynder.db", "location":"default"}
    db = window.sqlitePlugin.openDatabase(databaseDetails);
    console.log("done opening db");
    isMobile = true;

  }

  if (!db) {
    console.log("databse not opened!");
    return false;
  }
}
db.transaction(createTables);
getDislikedUsers();

function onReadyTransaction( ){
  console.log( 'Transaction completed' );
}
function createTables(transaction) {
  var sql = "CREATE TABLE IF NOT EXISTS userDislikes (idUser INTEGER, idDisliked INTEGER)";
  transaction.executeSql(sql, [], onSuccessExecuteSql, onError);
}
function onSuccessExecuteSql( tx, results ){
  console.log( 'Execute SQL completed' );
}
function onError( err ){
  console.log( "Error: " + err );
}

/* - getDislikedUsers - */
function getDislikedUsers(){
    db.transaction(
		function(tx){
			tx.executeSql( "SELECT * FROM userDislikes WHERE idUser = ?",
			[userLoogedId],
			buildDislikedArray,
			onError );
		},
		onError,
		onReadyTransaction
	);
}

function buildDislikedArray(tx, results){
    usersDisliked = [];
    if(results.rows.length == 0){
        console.log("No records found");
    }else{
        for(var i=0; i<results.rows.length; i++) {
            usersDisliked.push(results.rows.item(i).idDisliked);
        }
    }
    getUsersNearby();
}

function getUsersNearby(){
    usersNearby = [];
    for (var i = 0; i < usersDB.length; i++) {
        if (usersDB[i].location == userLocation) {
            if (usersDisliked.includes(usersDB[i].id)) {
                continue;
            }
            else {
                usersNearby.push(usersDB[i]);
            }
        }
    }
    startSearch();
}

function startSearch(){
    if (usersNearby.length < 1) {
        //hide
        document.getElementById("image-container").style.display = "none";
        document.getElementById("userName").innerHTML = "<strong>No one is nearby!</strong>";
        document.getElementById("userDescription").style.display = "none";
        document.getElementById("choicebtns").style.display = "none";



    }else{
        var pos = Math.floor(Math.random() * usersNearby.length);
        posCurrentUserProfile = pos;
        console.log("Selected pos: " + posCurrentUserProfile);
        console.log("Selected id: " + usersNearby[pos].id + ", ");
        document.getElementById("userImage").src = usersNearby[pos].photo;
        document.getElementById("userName").innerHTML = "<strong>" + usersNearby[pos].name + "</strong>" + ", " + usersNearby[pos].age;
        document.getElementById("userDescription").innerHTML = usersNearby[pos].description;
        //make them visible
        document.getElementById("image-container").style.display = "flex";
        document.getElementById("userDescription").style.display = "block";
        document.getElementById("choicebtns").style.display = "block";

    }
}

function likeUser() {
    console.log("LikeUser button");


    if (isMobile) {
        var myContact = navigator.contacts.create({"displayName": usersNearby[posCurrentUserProfile].name});
        var name = new ContactName();
        /*
        name.givenName = "Jane";
        name.familyName = "Doe";
        myContact.name = name;
        */
        var phoneNumbers = [];
        phoneNumbers[0] = new ContactField('mobile', '917-555-5432', true); // preferred number
        myContact.phoneNumbers = phoneNumbers;

        //var photos = [];
        //photos[0] = new ContactField('url', "///android_asset/www/img/0.jpg", true)
        //photos[0] = new ContactField('url', usersNearby[posCurrentUserProfile].photo, true)
        //myContact.photos = photos;

        myContact.save(onSuccessCallBack, onErrorCallBack);

        function onSuccessCallBack(contact) {
            alert("Save Success");
        };

        function onErrorCallBack(contactError) {
            log("Error = " + contactError.code);
        };



        /*-----------------------------------*/
        /*
        try {
            var x = navigator.contacts.create({"displayName":  usersNearby[posCurrentUserProfile].name});
            x.save();
            alert(usersNearby[posCurrentUserProfile].name +" saved to your contacts!");
        }
        catch (err) {
            alert("error ");
            console.log("ERROR " + err);
        }
        */
        getDislikedUsers();
        console.log("novo perfil");
    }else {
        alert("You only can save a contact using a mobile device!");
    }


}

function dislikeUser(transaction) {
    console.log("ID a Remover: " + usersNearby[posCurrentUserProfile].id);
    console.log("posCurrentUserProfile: " + posCurrentUserProfile);
    var toRemove = usersNearby[posCurrentUserProfile].id;
    db.transaction(function(transaction) {
        transaction.executeSql("SELECT * FROM userDislikes WHERE idUser = ? AND idDisliked = ?",
        [userLoogedId, toRemove],
        function (tx, results) {
            var numRows = results.rows.length;
            if (numRows<1) {
                var sql = "INSERT INTO userDislikes (idUser, idDisliked) VALUES (?,?)"
                transaction.executeSql(sql,[userLoogedId, toRemove], function(tx,result){

                }, function(error){
                    console.log("Insert failed: " + error);
                });
            }
        }, function(error){
        console.log("An error occured: " + error)
        });
    });

    getDislikedUsers();
    console.log("novo perfil");
}



document.getElementById("clicklogin").addEventListener("click", clicklogin)
function clicklogin() {
    window.location = "search.html";
}

document.getElementById("clickprofile").addEventListener("click", clickprofile)
function clickprofile() {
    window.location = "profile.html";
}

document.getElementById("clickpicture").addEventListener("click", clickpicture)
function clickpicture() {
    window.location = "pictureupload.html";
}
